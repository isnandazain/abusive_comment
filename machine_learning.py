import re
import time

import pickle
import pandas as pd
from string import punctuation

from sklearn.naive_bayes import GaussianNB
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

abusive_word = pd.read_csv("abusive.csv", header=0)
kamus_alay = pd.read_csv("kamus_alay.csv", header=None)

abusive_word = [abusive for abusive in abusive_word["ABUSIVE"]]

"""
METHOD :
- Data comment diterima
- Hapus Stopword dari comment
- Deteksi abusive comment, jika ditemukan maka delete comment
- Cocokkan, apakah rating sesuai dengan isi comment
- Tandai "sesuai" jika sesuai, Tandai "tidak sesuai" jika tidak sesuai
"""

sentence = 'Aku, pernah mendengar. Aisya bercerita bahwa sebenarnya ia tidak terlalu senang? dengan kabar - perjodohan! yang diatur oleh orang tuanya.'

# create factory
stop_factory = StopWordRemoverFactory()
more_stopword = ["dengan", "ia", "bahwa", "oleh"]

# tambahkan stopword baru
data = stop_factory.get_stop_words() + more_stopword
stopword = stop_factory.create_stop_word_remover()
sentence = stopword.remove(sentence)
sentence = sentence.translate(str.maketrans('', '', punctuation)).split(" ")

abusive_exist = []
for sen in sentence:
    if sen in abusive_word:
        abusive_exist.append(sen)

if len(abusive_exist) > 0:
    print("Comment Tidak Pantas")

else:
    print("Comment Pantas")